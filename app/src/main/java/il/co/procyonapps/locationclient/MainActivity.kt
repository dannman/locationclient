package il.co.procyonapps.locationclient

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import il.co.procyonapps.contract.LocationContract.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {

    private val locAdapter = LocAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        rvLocations.apply{
            this.adapter = locAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

        MainScope().launch {
            val list = getAllLocations()
            locAdapter.locations = list
        }

    }

    private suspend fun getAllLocations(): List<Location> = withContext(Dispatchers.IO) {
        val uri = Uri.parse("content://$AUTHORITY/$PATH")
        val result = mutableListOf<Location>()
        contentResolver.query(uri, null, null, null, null)
                ?.let {
                    var index = 0

                    while (!it.isLast) {
                        it.moveToPosition(index++)
                        val id = it.getInt(it.getColumnIndex(ID))
                        val lat = it.getDouble(it.getColumnIndex(LAT))
                        val lng = it.getDouble(it.getColumnIndex(LNG))
                        val timestamp = it.getString(it.getColumnIndex(TIMESTAMP))
                        result.add(Location(id, lat, lng, timestamp))


                    }
                    it.close()
                }
        return@withContext result
    }
}
