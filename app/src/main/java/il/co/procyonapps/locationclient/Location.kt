package il.co.procyonapps.locationclient

import java.sql.Timestamp

data class Location(val id: Int, val lat : Double, val lng: Double, val timestamp: String)