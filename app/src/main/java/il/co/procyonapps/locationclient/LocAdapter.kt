package il.co.procyonapps.locationclient

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_item.view.*

class LocAdapter : RecyclerView.Adapter<LocAdapter.LocationHolder>() {

    var locations = emptyList<Location>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class LocationHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(loc: Location) {
            view.tvIndex.text = "${loc.id})"
            view.tvLatLng.text = "${loc.lat}/${loc.lng}"
            view.tvTime.text = loc.timestamp
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationHolder {

        return LayoutInflater.from(parent.context).inflate(R.layout.row_item, parent, false)
            .let {
                LocationHolder(it)
            }
    }

    override fun getItemCount(): Int {
        return locations.size
    }

    override fun onBindViewHolder(holder: LocationHolder, position: Int) {
        holder.bind(locations[position])
    }
}